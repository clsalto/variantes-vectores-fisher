from keras.models import load_model
from keras.preprocessing.image import ImageDataGenerator
from sklearn.metrics import roc_auc_score, average_precision_score, f1_score, precision_score, recall_score, precision_recall_curve, confusion_matrix
import tensorflow as tf
import argparse

#To run with GPU, uncomment the following lines
#from keras.backend.tensorflow_backend import set_session
#config = tf.ConfigProto()
#config.gpu_options.per_process_gpu_memory_fraction = 0.4
#config.gpu_options.allow_growth = True
#set_session(tf.Session(config=config))

def get_args():
    parser = argparse.ArgumentParser(description='Test model in dataset')
    parser.add_argument('model_path', type=str, help="Model directory", default='')
    parser.add_argument('data_path', type=str, help="Data directory", default='')
    args = parser.parse_args()
    return args.model_path, args.data_path

def test_model(model_path, data_path):
    model = load_model(model_path)

    data_generator = ImageDataGenerator(rescale=1./255, samplewise_center = True, samplewise_std_normalization = True)

    test_generator = data_generator.flow_from_directory(
        data_path,
        target_size=(224, 224),
        batch_size=32,
        class_mode='binary',
        shuffle=False)

    labels = test_generator.labels
    predict = model.predict_generator(test_generator,steps = len(test_generator))

    predict_label = [1 if score > 0.5 else 0 for score in predict]

    roc = roc_auc_score(labels, predict)
    binary_cross_entropy, accuracy = model.evaluate_generator(test_generator, steps = len(test_generator))
    f1 = f1_score(labels, predict_label)
    precision = precision_score(labels, predict_label)
    recall = recall_score(labels, predict_label)
    average_precision = average_precision_score(labels, predict)
    tn, fp, fn, tp = confusion_matrix(labels, predict_label).ravel()

    print('*************************************************')
    print('model: {}'.format(model_path))
    print('data: {}'.format(data_path))
    print('binary cross entropy: {}'.format(binary_cross_entropy))
    print('accuracy: {}'.format(accuracy))
    print('roc: {}'.format(roc))
    print('f1: {}'.format(f1))
    print('precision: {}'.format(precision))
    print('recall: {}'.format(recall))
    print('average_precision: {}'.format(average_precision))
    print('tn: {}'.format(tn))
    print('fp: {}'.format(fp))
    print('fn: {}'.format(fn))
    print('tp: {}'.format(tp))

if __name__ == '__main__':
    model_path, data_path = get_args()
    test_model(model_path, data_path)