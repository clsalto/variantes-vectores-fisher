import os
import numpy as np
import random as rm
import tensorflow as tf
from resnet import set_optimizer, load_model_from_path, train_model, save_logs

#To run with GPU, uncomment the following lines
#from keras.backend.tensorflow_backend import set_session
#config = tf.ConfigProto()
#config.gpu_options.per_process_gpu_memory_fraction = 0.4
#config.gpu_options.allow_growth = True
#set_session(tf.Session(config=config))

def train_full_model(input_model_dir, train_dir, validation_dir, layer, p_optimizer, learning_rate, epochs, 
                    target_size, batch_size, step_rate_per_epoch, seed):
    # Set output parameters
    model_dir = '../../models/resnet50/weights/exp_{}_resnet_full_model.h5'.format(str(layer).zfill(2))
    logs_dir = '../../models/resnet50/logs/exp_{}_resnet_full_model.csv'.format(str(layer).zfill(2))
    print(model_dir)
    # Setting seeds
    os.environ['PYTHONHASHSEED']=str(seed)
    np.random.seed(seed)
    rm.seed(seed)
    #tf.set_random_seed(seed)

    # Set optimizer
    optimizer = set_optimizer(p_optimizer = p_optimizer, lr = learning_rate, decay = learning_rate/epochs)

    # Create model
    model = load_model_from_path(input_model_dir, optimizer, layer)

    # Train model
    model, history = train_model(model, train_dir, validation_dir, target_size, batch_size, epochs, step_rate_per_epoch)

    # Save model
    model.save(model_dir)

    # Save logs
    save_logs(model, logs_dir)

    return model_dir

if __name__ == '__main__':
    # Set parameters
    seed = 14
    target_size = (224, 224)
    epochs = 100
    p_optimizer = 'sgd'
    batch_size = 32
    learning_rate = 0.00001
    step_rate_per_epoch = 1
    train_dir = '../../data/images/modeling/resnet50/training/'
    validation_dir = '../../data/images/modeling/resnet50/validation/'

    # Make dirs
    os.makedirs('../../models/resnet50/logs/', exist_ok = True)
    os.makedirs('../../models/resnet50/weights/', exist_ok = True)

    # Set input model
    input_model_dir = '../../models/resnet50/weights/04_resnet_top_model.h5'

    for layer in [143, 81, 39, 0]:
        print('*****************************************************************************')
        print('*****************************************************************************')
        print('********************************  Layer {}  ********************************'.format(layer))
        print('*****************************************************************************')
        print('*****************************************************************************')
        input_model_dir = train_full_model(input_model_dir, train_dir, validation_dir, layer, p_optimizer, learning_rate, 
                                        epochs, target_size, batch_size, step_rate_per_epoch, seed)
