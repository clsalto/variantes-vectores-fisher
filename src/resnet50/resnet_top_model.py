import os
import numpy as np
import random as rm
import pandas as pd 
import tensorflow as tf
from resnet import set_optimizer, create_model, train_model, save_logs
import argparse

#To run with GPU, uncomment the following lines
#from keras.backend.tensorflow_backend import set_session
#config = tf.ConfigProto()
#config.gpu_options.per_process_gpu_memory_fraction = 0.4
#config.gpu_options.allow_growth = True
#set_session(tf.Session(config=config))

def get_args():
    parser = argparse.ArgumentParser(description='Ejecuta un experimento.')
    parser.add_argument('index', type=int, help="Experiment number", default=1)
    args = parser.parse_args()
    return args.index

if __name__ == '__main__':
    index = get_args()
    print('***************** Experiment number {} *****************'.format(index))

    # Set parameters
    train_directory = '../../data/images/modeling/resnet50/training/'
    val_directory = '../../data/images/modeling/resnet50/validation/'

    # Load data frame of parameters
    df_parameters = pd.read_csv('../../data/csv/parameters_resnet_top_model.csv', sep = ';', index_col = 0)

    # Set parameters
    seed = 14
    target_size=(224, 224)
    epochs = 100

    # Make dirs
    os.makedirs('../../models/resnet50/logs/', exist_ok = True)
    os.makedirs('../../models/resnet50/weights/', exist_ok = True)

    # Load variable parameters
    p_optimizer = df_parameters.at[index,'optimizer']
    batch_size = df_parameters.at[index,'batch_size']
    learning_rate = df_parameters.at[index,'learning_rate']

    # Set output parameters
    model_dir = '../../models/resnet50/weights/{}_resnet_top_model.h5'.format(str(index).zfill(2))
    logs_dir = '../../models/resnet50/logs/{}_logs.csv'.format(str(index).zfill(2))

    # Setting seeds
    os.environ['PYTHONHASHSEED']=str(seed)
    np.random.seed(seed)
    rm.seed(seed)
    #tf.set_random_seed(seed)

    # Set optimizer
    optimizer = set_optimizer(p_optimizer = p_optimizer, lr = learning_rate, decay = learning_rate/epochs)

    # Create model
    model = create_model(optimizer = optimizer)

    # Train model
    model, history = train_model(model, train_directory, val_directory, target_size, batch_size, epochs)

    # Save model
    model.save(model_dir)

    # Save logs
    save_logs(model, logs_dir)