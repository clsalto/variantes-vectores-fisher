import numpy as np
import pandas as pd 
import tensorflow as tf
from keras import models, regularizers, layers, optimizers
from keras.preprocessing.image import ImageDataGenerator
from keras.applications import ResNet50

def set_optimizer(p_optimizer, lr, decay):
    # Set optimizer
    if p_optimizer == 'adam':
        optimizer = optimizers.Adam(lr=lr)
    elif p_optimizer == 'sgd':
        optimizer = optimizers.SGD(lr=lr, momentum=0.9, decay=decay, nesterov = True)
    return optimizer

def create_model(optimizer):
    # Create model
    conv_base = ResNet50(weights='imagenet',
                        include_top=False,
                        input_shape=(224, 224, 3))

    model = models.Sequential()
    model.add(conv_base)
    model.add(layers.GlobalAveragePooling2D(data_format='channels_last'))
    model.add(layers.Dense(1, activation='sigmoid'))

    # Freeze layers
    for layer in conv_base.layers[:]:
        layer.trainable = False

    # Compile model
    model.compile(optimizer=optimizer,
                loss='binary_crossentropy',
                metrics=['accuracy'])

    return model

def train_model(model, train_dir, validation_dir, target_size, batch_size, epochs, step_rate_per_epoch=1):
    train_datagen = ImageDataGenerator(rescale=1./255,
                                    samplewise_center = True, 
                                    samplewise_std_normalization = True,
                                    rotation_range=40,
                                    width_shift_range=0.2,
                                    height_shift_range=0.2,
                                    zoom_range=0.2,
                                    horizontal_flip=True,
                                    vertical_flip=True,
                                    fill_mode='nearest')

    test_datagen = ImageDataGenerator(rescale=1./255,
                                    samplewise_center = True, 
                                    samplewise_std_normalization = True)

    train_generator = train_datagen.flow_from_directory(train_dir,
        target_size=target_size,batch_size=batch_size,class_mode='binary')
    validation_generator = test_datagen.flow_from_directory(
        validation_dir,target_size=target_size,batch_size=batch_size,class_mode='binary')

    Y = train_generator.labels
    class_weight = {}
    for l in set(Y):
        class_weight[l] = max([np.sum(Y == i) for i in np.unique(Y)])/np.sum(Y == l)
    
    # Train model
    history = model.fit_generator(train_generator,
                                epochs=epochs,
                                steps_per_epoch = int(len(train_generator)*step_rate_per_epoch),
                                validation_data = validation_generator,
                                validation_steps = len(validation_generator),
                                class_weight = class_weight)

    return model, history

def save_logs(model, logs_dir):
    valor_acc = 'acc' if 'acc' in model.history.history.keys() else 'accuracy'
    df = pd.DataFrame({'epoch':[1 + epoch for epoch in model.history.epoch],
              'loss':model.history.history['loss'],
              'acc':model.history.history['{}'.format(valor_acc)],
              'val_loss':model.history.history['val_loss'],
              'val_acc':model.history.history['val_{}'.format(valor_acc)],
             }, columns = ['epoch','loss','acc','val_loss','val_acc'])
    df.to_csv(logs_dir, sep = ';', encoding = 'UTF-8', index = False, decimal = ',')


def load_model_from_path(model_path, optimizer, layer=165):
    # Load model
    model = models.load_model(model_path)
    
    # Unfeeze layers
    for l in model.layers[0].layers[:layer]:
        l.trainable = False
    for l in model.layers[0].layers[layer:]:
        l.trainable = True
    for l in model.layers[1:]:
        l.trainable = True
    
    # Compile model
    model.compile(optimizer=optimizer,
                loss='binary_crossentropy',
                metrics=['accuracy'])

    return model