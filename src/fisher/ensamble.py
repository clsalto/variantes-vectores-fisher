import os
import pickle
from fisher import load_resnet_model, fisher_features_fusion, test_svm_fusion

if __name__ == '__main__':
    # Set parameters
    PATH = '../../data/images/modeling/test/'
    FISHER_WITH_PATCHES_PATH = '../../models/fisher_with_patches/model'
    FISHER_PATH = '../../models/fisher/model'
    RESNET50_MODEL_PATH = '../../models/resnet50/weights/exp_81_resnet_full_model.h5'
    TARGET_SIZE = (224,224)
    ANGLES = [0,90,180,270]
    MAX_PATCHES = 64

    # Fisher with patches
    SCALER_FISHER_WITH_PATCHES_PATH = os.path.join(FISHER_WITH_PATCHES_PATH, 'scaler.pickle')
    PCA_FISHER_WITH_PATCHES_PATH = os.path.join(FISHER_WITH_PATCHES_PATH, 'pca.pickle')
    GMM_FISHER_WITH_PATCHES_PATH = os.path.join(FISHER_WITH_PATCHES_PATH, 'gmm.pickle')
    SVM_FISHER_WITH_PATCHES_PATH = os.path.join(FISHER_WITH_PATCHES_PATH, 'svm.pickle')
    # Fisher
    SCALER_FISHER_PATH = os.path.join(FISHER_PATH, 'scaler.pickle')
    PCA_FISHER_PATH = os.path.join(FISHER_PATH, 'pca.pickle')
    GMM_FISHER_PATH = os.path.join(FISHER_PATH, 'gmm.pickle')
    SVM_FISHER_PATH = os.path.join(FISHER_PATH, 'svm.pickle')

    # Load objects
    # Fisher with patches
    fwp_model = load_resnet_model(RESNET50_MODEL_PATH, flatten_layer=True)
    with open(SCALER_FISHER_WITH_PATCHES_PATH, 'rb') as file:
        fwp_scaler = pickle.load(file)
    with open(PCA_FISHER_WITH_PATCHES_PATH, 'rb') as file:
        fwp_pca = pickle.load(file)
    with open(GMM_FISHER_WITH_PATCHES_PATH, 'rb') as file:
        fwp_gmm = pickle.load(file)
    with open(SVM_FISHER_WITH_PATCHES_PATH, 'rb') as file:
        fwp_clf = pickle.load(file)
    
    # Fisher
    f_model = load_resnet_model(RESNET50_MODEL_PATH)
    with open(SCALER_FISHER_PATH, 'rb') as file:
        f_scaler = pickle.load(file)
    with open(PCA_FISHER_PATH, 'rb') as file:
        f_pca = pickle.load(file)
    with open(GMM_FISHER_PATH, 'rb') as file:
        f_gmm = pickle.load(file)
    with open(SVM_FISHER_PATH, 'rb') as file:
        f_clf = pickle.load(file)

    # Get fisher vectors
    fv = fisher_features_fusion(folder=PATH, target_size=TARGET_SIZE,
                                model_1=f_model, scaler_1=f_scaler, pca_1=f_pca, 
                                gmm_1=f_gmm, 
                                angles_1=ANGLES, max_patches_1=0, patches_1=False,
                                model_2=fwp_model, scaler_2=fwp_scaler, pca_2=fwp_pca, 
                                gmm_2=fwp_gmm, 
                                angles_2=ANGLES, max_patches_2=MAX_PATCHES, patches_2=True)

    # Test models
    test_svm_fusion(f_clf, fwp_clf, fv)

    # CUDA_VISIBLE_DEVICES="1" python fusion.py