import argparse
import os
import pandas as pd
from fisher import load_resnet_model, generate_gmm, fisher_features, train_svm_cv

#To run with GPU, uncomment the following lines
#import tensorflow as tf
#from keras.backend.tensorflow_backend import set_session
#config = tf.ConfigProto()
#config.gpu_options.per_process_gpu_memory_fraction = 0.4
#config.gpu_options.allow_growth = True
#set_session(tf.Session(config=config))

def get_args():
    parser = argparse.ArgumentParser(description='Run experiment')
    parser.add_argument('index', type=int, help="Experiment number", default=1)
    args = parser.parse_args()
    return args

if __name__ == '__main__':
    args = get_args()
    index = args.index

    # Set input parameters
    DF_PARAMETERS_FILE = '../../data/csv/parameters_fisher.csv'
    FOLDER_TRAINING = '../../data/images/modeling/fisher/'
    MODEL_PATH = '../../models/resnet50/weights/exp_81_resnet_full_model.h5'
    TARGET_SIZE = (224, 224)
    ANGLES = [0,90,180,270]
    MAX_PATCHES_GMM = 64
    MAX_PATCHES_FISHER = 64
    LOGS_FOLDER = '../../models/fisher_with_patches/logs/'

    # Load data frame of parameters
    df_parameters = pd.read_csv(DF_PARAMETERS_FILE, sep = ';', index_col = 0)
    components = df_parameters.at[index,'components']
    k = df_parameters.at[index,'k']

    # Set output parameters
    logs_file = '{}{}_logs.csv'.format(LOGS_FOLDER, str(index).zfill(2))

    # Make dirs
    os.makedirs(LOGS_FOLDER, exist_ok = True)

    # Train cv model
    print('Part 1 load model')
    model = load_resnet_model(MODEL_PATH, flatten_layer=True)
    print('Part 2 generate gmm')
    scaler, pca, gmm = generate_gmm(FOLDER_TRAINING, k=k, model=model, components=components, target_size=TARGET_SIZE, angles=ANGLES, max_patches = MAX_PATCHES_GMM, patches=True)
    print('Part 3 fisher features training')
    fv_training = fisher_features(FOLDER_TRAINING, model=model, scaler=scaler, pca=pca, gmm=gmm, target_size=TARGET_SIZE, angles=ANGLES, max_patches = MAX_PATCHES_FISHER, patches=True)
    print('Part 4 training')
    train_svm_cv(fv_training, logs_file)