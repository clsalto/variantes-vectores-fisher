import argparse
import os
import pickle
from fisher import load_resnet_model, generate_gmm, fisher_features, train_svm, test_svm

#To run with GPU, uncomment the following lines
#import tensorflow as tf
#from keras.backend.tensorflow_backend import set_session
#config = tf.ConfigProto()
#config.gpu_options.per_process_gpu_memory_fraction = 0.4
#config.gpu_options.allow_growth = True
#set_session(tf.Session(config=config))

def get_args():
    parser = argparse.ArgumentParser(description='Train svm model from fisher vectors')
    parser.add_argument('components', type=int, help="PCA components", default=512)
    parser.add_argument('k', type=int, help="Gaussian components", default=32)
    parser.add_argument('C', type=int, help="SVM C param", default=1)
    parser.add_argument('kernel', type=str, help="SVM kernel param", default='linear')
    parser.add_argument('gamma', type=float, help="SVM gamma param", default=0.1)
    args = parser.parse_args()
    return args.components, args.k, args.C, args.kernel, args.gamma

if __name__ == '__main__':
    # Load arguments
    print('Load arguments')
    components, k, C, kernel, gamma = get_args()
    
    # Set input parameters
    FOLDER_TRAINING = '../../data/images/modeling/fisher/'
    FOLDER_TEST = '../../data/images/modeling/test/'
    MODEL_PATH = '../../models/resnet50/weights/exp_81_resnet_full_model.h5'
    TARGET_SIZE = (224, 224)
    ANGLES = [0,90,180,270]

    # Set output parameters
    FOLDER_MODEL_PATH = '../../models/fisher/model/'
    SCALER_PATH = os.path.join(FOLDER_MODEL_PATH, 'scaler.pickle')
    PCA_PATH = os.path.join(FOLDER_MODEL_PATH, 'pca.pickle')
    GMM_PATH = os.path.join(FOLDER_MODEL_PATH, 'gmm.pickle')
    SVM_PATH = os.path.join(FOLDER_MODEL_PATH, 'svm.pickle')

    # Make dirs
    os.makedirs(FOLDER_MODEL_PATH, exist_ok = True)

    # Train cv model
    print('Part 1 load model')
    model = load_resnet_model(MODEL_PATH)

    print('Part 2 generate gmm')
    if not ((os.path.exists(SCALER_PATH)) & (os.path.exists(PCA_PATH)) & (os.path.exists(GMM_PATH))):
        scaler, pca, gmm = generate_gmm(FOLDER_TRAINING, k=k, model=model, components=components, target_size=TARGET_SIZE, angles=ANGLES)
        with open(SCALER_PATH, 'wb') as file:
            pickle.dump(scaler, file)
        with open(PCA_PATH, 'wb') as file:
            pickle.dump(pca, file)
        with open(GMM_PATH, 'wb') as file:
            pickle.dump(gmm, file)
    else:
        with open(SCALER_PATH, 'rb') as file:
            scaler = pickle.load(file)
        with open(PCA_PATH, 'rb') as file:
            pca = pickle.load(file)
        with open(GMM_PATH, 'rb') as file:
            gmm = pickle.load(file)

    print('Part 3 train svm from fisher vectors')
    if not os.path.exists(SVM_PATH):
        fv_training = fisher_features(FOLDER_TRAINING, model=model, scaler=scaler, pca=pca, gmm=gmm, target_size=TARGET_SIZE, angles=ANGLES)
        clf = train_svm(fv_training, kernel, C, gamma)
        with open(SVM_PATH, 'wb') as file:
            pickle.dump(clf, file)
    else:
        with open(SVM_PATH, 'rb') as file:
            clf = pickle.load(file)

    print('Part 4 test model')
    fv_test = fisher_features(FOLDER_TEST, model=model, scaler=scaler, pca=pca, gmm=gmm, target_size=TARGET_SIZE, angles=ANGLES)
    test_svm(clf, fv_test)