import glob
import numpy as np
import os
import pandas as pd
from keras import models, layers
from keras.preprocessing import image
from sklearn import svm
from sklearn.decomposition import PCA
from sklearn.feature_extraction.image import extract_patches_2d
from sklearn.metrics import accuracy_score, roc_auc_score, average_precision_score, f1_score, precision_score, \
                            recall_score, precision_recall_curve, confusion_matrix, make_scorer
from sklearn.mixture import GaussianMixture
from sklearn.model_selection import GridSearchCV
from sklearn.preprocessing import MinMaxScaler

def load_resnet_model(model_path, layer_name='activation_49', flatten_layer=False):
    # Load model
    conv_base = models.load_model(model_path).layers[0]

    if flatten_layer:
        model = models.Sequential()
        model.add(models.Model(inputs=conv_base.inputs[0], outputs=conv_base.get_layer(layer_name).output))
        model.add(layers.Flatten())
    else:
        model = models.Model(inputs=conv_base.inputs[0], outputs=conv_base.get_layer(layer_name).output)
    
    return model

def image_descriptors(file, model, target_size = (224, 224), angles = [0,90,180,270], max_patches = 16, patches=False):
    if patches:
        img_data = image.load_img(file)
        d = []
        for angle in angles:
            img_data_rotate = img_data.rotate(angle, expand=True)
            img_data_array = image.img_to_array(img_data_rotate)
            patches = extract_patches_2d(img_data_array, target_size, max_patches = max_patches)
            for patch in patches:
                img = np.expand_dims(patch, axis=0)
                img = img*1./255
                img = img-np.mean(img)
                descriptors = model.predict(img)
                d.append(descriptors)
    else:
        img_data = image.load_img(file, target_size=target_size)
        x_shape, y_shape = model.output_shape[1]*model.output_shape[2], model.output_shape[3]
        d = []
        for angle in angles:
            img_data_rotate = img_data.rotate(angle, expand=True)
            img_data_array = image.img_to_array(img_data_rotate)
            img = np.expand_dims(img_data_array, axis=0)
            img = img*1./255
            img = img-np.mean(img)
            descriptors = model.predict(img).reshape(x_shape, y_shape)
            d.append(descriptors)
    return np.concatenate(d)

def folder_descriptors(directory, model, target_size = (224, 224), angles = [0,90,180,270], max_patches=16, patches=False):
    if os.path.isdir(directory):
        return np.concatenate([folder_descriptors(os.path.join(directory, subdir), model, target_size, angles, max_patches, patches)\
                               for subdir in os.listdir(directory)])
    else:
        return image_descriptors(directory, model, target_size, angles, max_patches, patches)

def dictionary(descriptors, N):
    gmm = GaussianMixture(n_components=N, covariance_type='full')
    gmm.fit(descriptors)
    return gmm

def generate_gmm(input_folder, k,  model, components, target_size, angles, max_patches=16, patches=False):
    print("Training GMM of size", k)
    print("get words")
    words = folder_descriptors(input_folder, model, target_size, angles, max_patches, patches)

    print("get scaler")
    scaler = MinMaxScaler()
    scaler.fit(words)
    words = scaler.transform(words)
    
    print("get PCA")
    pca = PCA(n_components=components)
    pca.fit(words)
    words = pca.transform(words)
    
    print("get gmm")
    gmm = dictionary(words, k)
    
    return scaler, pca, gmm

def likelihood_moment(x, ytk, moment):
    x_moment = np.power(np.float32(x), moment) if moment > 0 else np.float32([1])
    return x_moment * ytk

def likelihood_statistics(samples, gmm):
    gaussians, s0, s1,s2 = {}, {}, {}, {}

    proba = gmm.predict_proba(samples)
    zip_proba = zip(range(0, len(proba)), proba)
    for index, x in zip_proba:
        gaussians[index] = x
    
    for k in range(0, len(gmm.weights_)):
        s0[k], s1[k], s2[k] = 0, 0, 0
        zipsamples = zip(range(0, len(samples)), samples)
        for index, x in zipsamples:
            probabilities = np.multiply(gaussians[index], gmm.weights_)
            probabilities = probabilities / np.sum(probabilities)
            s0[k] = s0[k] + likelihood_moment(x, probabilities[k], 0)
            s1[k] = s1[k] + likelihood_moment(x, probabilities[k], 1)
            s2[k] = s2[k] + likelihood_moment(x, probabilities[k], 2)
    return s0, s1, s2

def fisher_vector_weights(s0, s1, s2, means, covs, w, T):
    return np.float32([((s0[k] - T * w[k]) / np.sqrt(w[k]) ) for k in range(0, len(w))])

def fisher_vector_means(s0, s1, s2, means, sigma, w, T):
    return np.float32([(s1[k] - means[k] * s0[k]) / (np.sqrt(w[k] * sigma[k])) for k in range(0, len(w))])

def fisher_vector_sigma(s0, s1, s2, means, sigma, w, T):
    return np.float32([(s2[k] - 2 * means[k]*s1[k]  + (means[k]*means[k] - sigma[k]) * s0[k]) / (np.sqrt(2*w[k])*sigma[k])  for k in range(0, len(w))])

def normalize(fisher_vector):
    v = np.sqrt(abs(fisher_vector)) * np.sign(fisher_vector)
    return v / np.sqrt(np.dot(v, v))

def fisher_vector(samples, gmm):
    s0, s1, s2 =  likelihood_statistics(samples,gmm)
    T = samples.shape[0]
    # Set means, covs and weights
    means = gmm.means_
    covs = np.float32([np.diagonal(gmm.covariances_[k]) for k in range(0, gmm.covariances_.shape[0])])
    w = gmm.weights_
    a = fisher_vector_weights(s0, s1, s2, means, covs, w, T)
    b = fisher_vector_means(s0, s1, s2, means, covs, w, T)
    c = fisher_vector_sigma(s0, s1, s2, means, covs, w, T)
    fv = np.concatenate([np.concatenate(a), np.concatenate(b), np.concatenate(c)])
    fv = normalize(fv)
    return fv

def get_fisher_vectors_from_folder(folder, model, scaler, pca, gmm, target_size, angles, max_patches=16, patches=False):
    files = glob.glob(folder + "/*.jpg")
    return np.float32([fisher_vector(pca.transform(scaler.transform(image_descriptors(f, model, target_size, angles, max_patches, patches))), gmm)\
                       for f in files])

def fisher_features(folder, model, scaler, pca, gmm, target_size, angles, max_patches=16, patches=False):
    folders = glob.glob(folder + "/*")
    folders.sort()
    features = {f : get_fisher_vectors_from_folder(f, model, scaler, pca, gmm, target_size, angles, max_patches, patches) for f in folders}
    return features

def get_fusion_fisher_vectors_from_folder(folder, target_size,
                                          model_1, scaler_1, pca_1, gmm_1, angles_1, max_patches_1, patches_1,
                                          model_2, scaler_2, pca_2, gmm_2, angles_2, max_patches_2, patches_2):
    files = glob.glob(folder + "/*.jpg")

    return [[fisher_vector(pca_1.transform(scaler_1.transform(image_descriptors(f, model_1, target_size, angles_1, max_patches_1, patches_1))), gmm_1),\
        fisher_vector(pca_2.transform(scaler_2.transform(image_descriptors(f, model_2, target_size, angles_2, max_patches_2, patches_2))), gmm_2)]\
        for f in files]

def fisher_features_fusion(folder, target_size,
                          model_1, scaler_1, pca_1, gmm_1, angles_1, max_patches_1, patches_1,
                          model_2, scaler_2, pca_2, gmm_2, angles_2, max_patches_2, patches_2):
    folders = glob.glob(folder + "/*")
    folders.sort()
    features = {f : get_fusion_fisher_vectors_from_folder(f, target_size,
                                          model_1, scaler_1, pca_1, gmm_1, angles_1, max_patches_1, patches_1,
                                          model_2, scaler_2, pca_2, gmm_2, angles_2, max_patches_2, patches_2) for f in folders}
    return features

def tn(y_true, y_pred): return confusion_matrix(y_true, y_pred)[0, 0]
def fp(y_true, y_pred): return confusion_matrix(y_true, y_pred)[0, 1]
def fn(y_true, y_pred): return confusion_matrix(y_true, y_pred)[1, 0]
def tp(y_true, y_pred): return confusion_matrix(y_true, y_pred)[1, 1]

def train_svm_cv(features, logs_dir):
    X = np.concatenate(list(features.values()))
    Y = np.concatenate([np.float32([i]*len(v)) for i,v in zip(range(0, len(features)), list(features.values()))])

    svc = svm.SVC()
    
    cw = {}
    for l in set(Y):
        cw[l] = max([np.sum(Y == i) for i in np.unique(Y)])/np.sum(Y == l)

    param_grid = {"kernel" : ['poly', 'rbf'],
                  "gamma" : [1e-1, 1e-2, 1e-3, 1e-4],
                  "C" : [1, 10, 100, 1000],
                  "class_weight": [cw]}

    scoring = {'tp': make_scorer(tp), 
               'tn': make_scorer(tn),
               'fp': make_scorer(fp), 
               'fn': make_scorer(fn),
               'AUC': 'roc_auc', 
               'Accuracy': 'accuracy',
               'Average_Precision': 'average_precision',
               'Recall': 'recall',
               'Precision': 'precision',
               'F1': 'f1'}

    CV_svc = GridSearchCV(estimator=svc, 
                          param_grid=param_grid, 
                          scoring=scoring,
                          cv= 5,
                          n_jobs=4,
                          refit='AUC')

    CV_svc.fit(X, Y)

    df = pd.DataFrame.from_dict(CV_svc.cv_results_)
    df.to_csv(logs_dir, sep = ';', encoding = 'UTF-8', index = False, decimal = ',')

def train_svm(features, kernel, C, gamma):
    X = np.concatenate(list(features.values()))
    Y = np.concatenate([np.float32([i]*len(v)) for i,v in zip(range(0, len(features)), list(features.values()))])

    clf = svm.SVC(probability = True, kernel=kernel, C=C, gamma=gamma)       
    clf.fit(X, Y)

    return clf

def test_svm(clf, features):
    X = np.concatenate(list(features.values()))
    labels = np.concatenate([np.float32([i]*len(v)) for i,v in zip(range(0, len(features)), list(features.values()))])

    predict = clf.predict_proba(X).T[1]
    predict_label = [1 if score > 0.5 else 0 for score in predict]

    accuracy = accuracy_score(labels, predict_label)
    roc = roc_auc_score(labels, predict)
    f1 = f1_score(labels, predict_label)
    precision = precision_score(labels, predict_label)
    recall = recall_score(labels, predict_label)
    average_precision = average_precision_score(labels, predict)
    tn, fp, fn, tp = confusion_matrix(labels, predict_label).ravel()

    print('*************************************************')
    print('accuracy: {}'.format(accuracy))
    print('roc: {}'.format(roc))
    print('f1: {}'.format(f1))
    print('precision: {}'.format(precision))
    print('recall: {}'.format(recall))
    print('average_precision: {}'.format(average_precision))
    print('tn: {}'.format(tn))
    print('fp: {}'.format(fp))
    print('fn: {}'.format(fn))
    print('tp: {}'.format(tp))


def test_svm_fusion(clf1, clf2, features):
    X1 = np.float32([i[0] for i in np.concatenate(list(features.values()))])
    X2 = np.float32([i[1] for i in np.concatenate(list(features.values()))])
    labels = np.concatenate([np.float32([i]*len(v)) for i,v in zip(range(0, len(features)), list(features.values()))])

    predict1 = clf1.predict_proba(X1).T[1]
    predict2 = clf2.predict_proba(X2).T[1]

    #predict1 = [0.1, 0.2, 0.7, 0.4, 0.1, 0.2, 0.7, 0.4, 0.1, 0.2, 0.7, 0.4, 0.1, 0.2, 0.7, 0.4, 0.1, 0.2]
    #predict2 = [0.1, 0.2, 0.7, 0.6, 0.1, 0.2, 0.7, 0.6, 0.1, 0.2, 0.7, 0.6, 0.1, 0.2, 0.7, 0.6, 0.1, 0.2]

    predict = np.average(np.array([predict1, predict2]), axis=0)
    predict_label = [1 if score > 0.5 else 0 for score in predict]

    accuracy = accuracy_score(labels, predict_label)
    roc = roc_auc_score(labels, predict)
    f1 = f1_score(labels, predict_label)
    precision = precision_score(labels, predict_label)
    recall = recall_score(labels, predict_label)
    average_precision = average_precision_score(labels, predict)
    tn, fp, fn, tp = confusion_matrix(labels, predict_label).ravel()

    print('*************************************************')
    print('accuracy: {}'.format(accuracy))
    print('roc: {}'.format(roc))
    print('f1: {}'.format(f1))
    print('precision: {}'.format(precision))
    print('recall: {}'.format(recall))
    print('average_precision: {}'.format(average_precision))
    print('tn: {}'.format(tn))
    print('fp: {}'.format(fp))
    print('fn: {}'.format(fn))
    print('tp: {}'.format(tp))