import argparse
import cv2
import numpy as np
import os
import pandas as pd
import requests
import shutil
import zipfile
from keras.preprocessing.image import load_img, ImageDataGenerator, img_to_array
from sklearn.model_selection import train_test_split

def download_url(url, save_path, chunk_size=128):
    r = requests.get(url, stream=True)
    with open(save_path, 'wb') as fd:
        for chunk in r.iter_content(chunk_size=chunk_size):
            fd.write(chunk)

def zip_extractall(zip_path, extract_path):
    password = None
    archivo_zip = zipfile.ZipFile(zip_path, "r")
    archivo_zip.extractall(pwd=password, path=extract_path)
    archivo_zip.close()
    

def copy_images(df, folder_input, folder_output):
    for vclass in df['image_class'].unique():
        if not os.path.exists(os.path.join(folder_output, vclass)):
            os.makedirs(os.path.join(folder_output, vclass))
            
        folder_class = os.path.join(folder_output, vclass)
        df_class = df[df['image_class'] == vclass]

        for index, row in df_class.iterrows():
            src = os.path.join(folder_input,row['image_path'])
            dst = os.path.join(folder_class,row['image_path'])
            shutil.copy(src, dst)
            
def select_images(file_path, folder_input, folder_output, split_train_test = False):
    # Load images metadata
    df_metadata = pd.read_csv(file_path, 
                 names=['image_name', 'image_class'])
    df_metadata['image_class'] = np.where((df_metadata.image_class == 1) | (df_metadata.image_class == 'malignant'), 
                                          'malignant', 'benign')
    # Load images in disk
    list_images = [f for f in os.listdir(folder_input) if os.path.isfile(os.path.join(folder_input, f))]
    list_ids = [x.split('.')[0] for x in list_images]
    df_images = pd.DataFrame({'image_name':list_ids, 'image_path':list_images})
    
    # Join data frames
    df = pd.merge(df_metadata, df_images, how = 'inner', on = 'image_name')
    
    if split_train_test:
        #Split train and val
        X_train, X_val, y_train, y_val = train_test_split(df['image_path'], df['image_class'], test_size = 0.2, random_state = 42)
        train = pd.DataFrame({'image_path': X_train, 'image_class':y_train})
        val = pd.DataFrame({'image_path': X_val, 'image_class':y_val})
        #Copy images
        copy_images(train, folder_input, os.path.join(folder_output, 'training'))
        copy_images(val, folder_input, os.path.join(folder_output, 'validation'))
    else:
        #Copy images
        copy_images(df, folder_input, folder_output)

def image_rotate(folder_input, file_input, folder_output, thetas):
    file_name = file_input.split('.')[0]
    file_extension = file_input.split('.')[1]
    img = load_img(os.path.join(folder_input, file_input))
    data = img_to_array(img)
    img_gen = ImageDataGenerator()
    for theta in thetas:
        batch = img_gen.apply_transform(data, {'theta':theta})
        image = batch.astype('uint8')
        image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)
        cv2.imwrite(os.path.join(folder_output, '{}_{}.{}'.format(file_name, str(theta), file_extension)), image) 

def folder_rotation(folder_input, folder_output, thetas):
    onlyfiles = [f for f in os.listdir(folder_input) if os.path.isfile(os.path.join(folder_input, f))]
    if not os.path.exists(folder_output):
        os.makedirs(folder_output)
    for file in onlyfiles:
        image_rotate(folder_input, file, folder_output, thetas)

def get_args():
    parser = argparse.ArgumentParser(description='Download and preprocess data')
    parser.add_argument('folder_output', type=str, help="Folder output", default='../../data/')
    args = parser.parse_args()
    return args.folder_output

if __name__ == '__main__':
    FOLDER_OUTPUT = get_args()

    # Set url to download data
    print('Set url to download data')
    TRAINING_DATA_URL = 'https://isic-challenge-data.s3.amazonaws.com/2016/ISBI2016_ISIC_Part3_Training_Data.zip'
    TRAINING_LABEL_URL = 'https://isic-challenge-data.s3.amazonaws.com/2016/ISBI2016_ISIC_Part3_Training_GroundTruth.csv'
    TEST_DATA_URL = 'https://isic-challenge-data.s3.amazonaws.com/2016/ISBI2016_ISIC_Part3_Test_Data.zip'
    TEST_LABEL_URL = 'https://isic-challenge-data.s3.amazonaws.com/2016/ISBI2016_ISIC_Part3_Test_GroundTruth.csv'
    
    # Set output folders
    print('Set output folders')
    FOLDER_OUTPUT_ZIP = os.path.join(FOLDER_OUTPUT, 'zip')
    FOLDER_OUTPUT_CSV = os.path.join(FOLDER_OUTPUT, 'csv')
    FOLDER_OUTPUT_ROW = os.path.join(FOLDER_OUTPUT, 'images/row')
    FOLDER_OUTPUT_PREPROCESING = os.path.join(FOLDER_OUTPUT, 'images/preprocesing/')
    FOLDER_OUTPUT_MODELING_RESNET50 = os.path.join(FOLDER_OUTPUT, 'images/modeling/resnet50/')
    FOLDER_OUTPUT_MODELING_FISHER = os.path.join(FOLDER_OUTPUT, 'images/modeling/fisher/')
    FOLDER_OUTPUT_TEST = os.path.join(FOLDER_OUTPUT, 'images/modeling/test/')

    # Create folders
    print('Create folders')
    os.makedirs(FOLDER_OUTPUT_ZIP, exist_ok = True)
    os.makedirs(FOLDER_OUTPUT_CSV, exist_ok = True)
    os.makedirs(FOLDER_OUTPUT_ROW, exist_ok = True)
    os.makedirs(FOLDER_OUTPUT_PREPROCESING, exist_ok = True)
    os.makedirs(FOLDER_OUTPUT_MODELING_RESNET50, exist_ok = True)
    os.makedirs(FOLDER_OUTPUT_MODELING_FISHER, exist_ok = True)
    os.makedirs(FOLDER_OUTPUT_TEST, exist_ok = True)

    # Download data
    print('Download data')
    download_url(TRAINING_DATA_URL, os.path.join(FOLDER_OUTPUT_ZIP, 'ISBI2016_ISIC_Part3_Training_Data.zip'))
    download_url(TEST_DATA_URL, os.path.join(FOLDER_OUTPUT_ZIP, 'ISBI2016_ISIC_Part3_Test_Data.zip'))
    download_url(TRAINING_LABEL_URL, os.path.join(FOLDER_OUTPUT_CSV, 'ISBI2016_ISIC_Part3_Training_GroundTruth.csv'))
    download_url(TEST_LABEL_URL, os.path.join(FOLDER_OUTPUT_CSV, 'ISBI2016_ISIC_Part3_Test_GroundTruth.csv'))

    # Extract data
    print('Extract data')
    zip_extractall(os.path.join(FOLDER_OUTPUT_ZIP, 'ISBI2016_ISIC_Part3_Training_Data.zip'), FOLDER_OUTPUT_ROW)
    zip_extractall(os.path.join(FOLDER_OUTPUT_ZIP, 'ISBI2016_ISIC_Part3_Test_Data.zip'), FOLDER_OUTPUT_ROW)

    # Select images
    print('Select images')
    select_images(file_path=os.path.join(FOLDER_OUTPUT_CSV, 'ISBI2016_ISIC_Part3_Training_GroundTruth.csv'), 
                folder_input=os.path.join(FOLDER_OUTPUT_ROW, 'ISBI2016_ISIC_Part3_Training_Data/'), 
                folder_output=os.path.join(FOLDER_OUTPUT_PREPROCESING, 'select/'), 
                split_train_test = True)
    select_images(file_path=os.path.join(FOLDER_OUTPUT_CSV, 'ISBI2016_ISIC_Part3_Training_GroundTruth.csv'), 
                folder_input=os.path.join(FOLDER_OUTPUT_ROW, 'ISBI2016_ISIC_Part3_Training_Data/'), 
                folder_output=FOLDER_OUTPUT_MODELING_FISHER, 
                split_train_test = False)
    select_images(file_path=os.path.join(FOLDER_OUTPUT_CSV, 'ISBI2016_ISIC_Part3_Test_GroundTruth.csv'),
                folder_input=os.path.join(FOLDER_OUTPUT_ROW, 'ISBI2016_ISIC_Part3_Test_Data/'),
                folder_output=FOLDER_OUTPUT_TEST, 
                split_train_test = False)

    # Rotate images
    print('Rotate images')
    folder_rotation(os.path.join(FOLDER_OUTPUT_PREPROCESING, 'select/training/benign/'), 
                    os.path.join(FOLDER_OUTPUT_MODELING_RESNET50, 'training/benign/'), 
                    [0,90,180,270])
    folder_rotation(os.path.join(FOLDER_OUTPUT_PREPROCESING, 'select/training/malignant/'), 
                    os.path.join(FOLDER_OUTPUT_MODELING_RESNET50, 'training/malignant/'), 
                    [0,90,180,270])
    folder_rotation(os.path.join(FOLDER_OUTPUT_PREPROCESING, 'select/validation/benign/'), 
                    os.path.join(FOLDER_OUTPUT_MODELING_RESNET50, 'validation/benign/'), 
                    [0,90,180,270])
    folder_rotation(os.path.join(FOLDER_OUTPUT_PREPROCESING, 'select/validation/malignant/'), 
                    os.path.join(FOLDER_OUTPUT_MODELING_RESNET50, 'validation/malignant/'), 
                    [0,90,180,270])