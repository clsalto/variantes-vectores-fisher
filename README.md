# Variantes de Vectores de Fisher para la clasificación de imágenes de lesiones de piel mediante redes neuronales profundas residuales

## Resumen 📋

El presente trabajo investiga alternativas al problema de clasificación de imágenes dermatoscópicas utilizando redes neuronales residuales (ResNet) y codificando los descriptores de la misma con Vectores de Fisher. En primer lugar se realizó el reentrenamiento de un clasificador ResNet-50 (ResNet-50-FT). Luego se aplicaron Vectores de Fisher a partir de los descriptores de distintas muestras de una imagen (ResNet-50-FV-CM). Otra alternativa investigada fue generar Vectores de Fisher sobre la base de los descriptores obtenidos como salida del quinto bloque convolucional de la red ResNet-50 (ResNet-50-FV). Finalmente se realizó un ensamble (FV-Ensamble) de las aplicaciones de Vectores de Fisher logrando resultados acorde a lo desarrollado en otros trabajos.

## Resultados obtenidos 📌

En todas las aplicaciones se utilizaron los datos de la competencia [ISIC 2016](https://arxiv.org/abs/1605.01397). Este conjunto de datos se compone de 900 imágenes de entrenamiento y 379 imágenes de prueba. El primer conjunto se divide en 727 imágenes de lesiones benignas y 179 melanomas. En tanto que el conjunto de prueba se conforma a partir de 304 lesiones benignas y 75 melanomas. El cuadro contiene los resultados de los métodos propuestos y otros trabajos que utilizaron el mismo conjunto de datos de prueba. Para comparar los distintos métodos se utilizaron las métricas: Precisión media (mAP), Accuracy (Acc) y Área bajo la curva ROC (AUC). De las tres primeras alternativas planteadas, los Vectores de Fisher procesados a partir de los descriptores del quinto bloque convolucional (ResNet-50-FV) son los que mejor rendimiento tienen analizando las tres métricas. Si bien el ensamble propuesto es superior en las métricas mAP y AUC a los modelos originales y al mejor clasificador de la competencia ISBI 2016, existen otras aplicaciones desarrolladas que lograron mejores rendimientos a ambos trabajos.

|Método|Red|mAP|Acc|AUC|
|---|---|---|---|---|
|ResNet-50-FT| ResNet-50| 58.52| 82.58| 79.62|
|ResNet-50-FV-CM| ResNet-50| 54.62| 84.16| 75.58|
|ResNet-50-FV| ResNet-50| 61.34| 84.69| 80.93|
|FV-Ensamble| ResNet-50| 64.46| 83.90| 82.95|
|[DCNN-FV](https://ieeexplore.ieee.org/document/8440053)| ResNet-50| 68.49| **86.81**| 85.20|
|[DCNN-FV](https://ieeexplore.ieee.org/document/7950524)| AlexNet-FC6| 59.50| 83.09| 79.57|
|[LDF-FV](https://www.researchgate.net/publication/319578126_Aggregating_Deep_Convolutional_Features_for_Melanoma_Recognition_in_Dermoscopy_Images)| ResNet-50| 68.49| **86.81**| 85.20|
|[CUMED](https://ieeexplore.ieee.org/document/7792699)| | 63.70| 85.50| 80.40|
|[SLD](https://www.researchgate.net/publication/331184944_Medical_Image_Classification_Using_Synergic_Deep_Learning)| | 68.10| 86.30| 82.20|
|[M-CNN-FV](https://www.researchgate.net/publication/340494047_Convolutional_descriptors_aggregation_via_cross-net_for_skin_lesion_recognition)| | **68.73**| **86.81**| **85.82**|
|[VGG-16-FV](https://bibliotecadigital.exactas.uba.ar/collection/paper/document/paper_03029743_v11401LNCS_n_p732_Liberman)| Ensamble| 68.13| 80.29| 82.11|

## Desarrollo realizado ⌨️

### Contenido del proyecto

La organización del proyecto sigue la siguiente estructura:

```
.
├── data/                         # Datos
│   ├── csv/                      # Archivos csv con los hiperparámetros de entrenamiento y archivos de preparacion de datos
│   ├── images/                   # Carpeta generada al momento de descargar las imágenes y prepararlas para el entrenamiento
│   └── zip/                      # Carpeta generada al momento de descargar las imágenes desde el origen
├── docs/                         # Documentación del proyecto
├── models/                       # Carpeta que contiene los modelos entrenados
│   ├── fisher/                   # Logs de entrenamiento y modelo de Vectores de Fisher a partir de descriptores ResNet-50
│   │    ├── logs/                # Logs del modelo de Vectores de Fisher a partir de los descriptores de la ResNet-50
│   │    └── model/               # Modelo de escalamiento, PCA, GMM y clasificador SVM
│   ├── fisher_with_patches/      # Logs de entrenamiento y modelo de Vectores de Fisher con muestras
│   │    ├── logs/                # Logs del modelo de Vectores de Fisher con muestras
│   │    └── model/               # Modelo de escalamiento, PCA, GMM y clasificador SVM
│   └── resnet50/                 # Logs de entrenamiento y ResNet-50
│   │    ├── logs/                # Logs de entrenamiento
│   │    └── weights/             # Modelos entrenados
├── notebooks/                    # Cuadernos con análisis de los resultados de los experimentos
├── src/                          # Código de las aplicaciones
│   ├── fisher/                   # Scripts para entrenar y testear los modelos de Vectores de Fisher
│   ├── preprocesing/             # Scripts de preparación de datos
│   └── resnet50/                 # Scripts para entrenar y testear los modelos de ResNet-50
├── .gitignore                    # Contiene los archivos a ignorar en el versionado del código
├── requirements.txt              # Librerias para instalar
└── README.md                     # Guia del proyecto
```

### Version de Python y librerías necesarias

El código fue desarrollado en Python 3.6.8, por lo que se sugiere usar esta version o una posterior. Antes de ejecutar algun código del proyecto es necesario instalar las librerias. Las mismas estan detalladas en el archivo `requirements.txt`. Este archivo por defecto contiene los paquetes necesarios para correr el código en CPU, en caso de ser necesario ejecutarlo en GPU se aconseja reemplazar las librerias de keras y tensorflow comentadas en el archivo y suprimir las de estas librerias que vienen por defecto. 

```
pip install -r requirements.txt
```

### Preparación de datos

La etapa de preparación de datos consiste en descargar las imágenes desde el repositorio de ISIC, descomprimirlas, agruparlas de acuerdo a su clase y rotarlas. Todo esto es realizado por el script `/src/preprocesing/preprocesing.py`, el mismo requiere un parámetro que indique la carpeta donde se van almacenar todos estos datos. A continuación se presenta un ejemplo como ejecutar este script.

```
cd /src/preprocesing
python preprocesing.py ../../data/
```

### ResNet-50 (ResNet-50-FT)

A partir de la red entrenada con ImageNet, se realizó transferencia de aprendizaje. Para adaptar este modelo a la aplicación actual fue necesario reemplazar la última capa totalmente conectada por una capa de una única neurona de salida con activación sigmoide. Luego la red fue entrenada por etapas descongelando los pesos de las neuronas desde la capa superior hasta las capas menores.

#### Entrenamiento de la capa totalmente conectada

Para entrenar la capa totalmente conectada se usaron los siguientes hiperparámetros:

* optimizer = {adam, sgd}
* batch_size = {16, 32}
* learning_rate = {0.001, 0.0001} 

Estos hiperparámetros estan especificados en el archivo `/data/csv/parameters_resnet_top_model.csv`. Para realizar alguna de estas pruebas es necesario ejecutar el script `/src/resnet50/resnet_top_model.py` indicando el número de experimento.

```
cd /src/resnet50
python resnet_top_model.py 1
```

En caso de desar ejecutar el script con GPU es necesario descomentar el codigo comentado en dicho archivo. Esto mismo aplica para los otros scripts desarrollados.

#### Entrenamiento de los bloques convolucionales

El entrnamiento de los bloques convolucionales se realiza de manera iterativa desde el ultimo bloque hasta el primero. Dentro del scrip se especifica la ubicación del modelo original al cual se realiza el entrenamiento. El script que realiza esta tarea es `/src/resnet50/resnet_full_model.py`.

```
cd /src/resnet50
python resnet_full_model.py
```

#### Validacion de los modelos entrenados

Para validar el rendimiento de alguno de los modelos en el conjunto de datos de prueba es necesario ejecutar el script `/src/resnet50/resnet_test_model.py` indicando el modelo a probar y el conjunto de datos de prueba.

```
cd /src/resnet50
python resnet_test_model.py ../../models/resnet50/weights/exp_81_resnet_full_model.h5 ../../data/images/modeling/test/
```

### Vectores de Fisher con muestras (ResNet-50-FV-CM)

El modelo de Vectores de Fisher con muestras consiste en representar una imagen de lesión cutánea como múltiples subimágenes y extraer las características profundas de estas subimágenes. Para extraer las características profundas se utilizó la red previamente entrenada, ResNet-50-FT, a la cual se reemplazó las capas de agrupación promedio y activación sigmoide por una capa plana. Luego, los vectores de características son escalados y su dimensionalidad es reducida mediante PCA. A continuación se entrena un modelo de mixtura de gaussianas (GMM) con K componentes. Con los descriptores de una imagen y el modelo GMM se obtienen los FV aplicando el algoritmo planteado en [Image Classification with the Fisher Vector: Theory and Practice. International Journal of Computer Vision](https://hal.inria.fr/hal-00830491v2/document). Para implmentar el mismo se partio del desarrollo realizado en https://github.com/jacobgil/pyfishervector. Finalmente con los FV se entrena un clasificador SVM.

Para entrenar los modelos de PCA y GMM se realizaron utilizaron los siguientes parámetros:
* número de componentes principales = {32, 64, 128, 256, 384, 512}
* número de componentes gaussianas = {16, 32, 64, 100}

Mientras que el clasificador SVM se entreno usando validación cruzada con los siguientes parámetros:
* C = {1, 10, 100, 1000}
* kernel = {poly, rbf}
* gamma = {1e-1, 1e-2, 1e-3, 1e-4}

Los hiperparámetros de PCA y GMM estan especificados en el archivo `/data/csv/parameters_fisher.csv`. Para realizar algunas de las pruebas es necesario ejecutar el script `/src/fisher/train_cv_svm_with_patches.py` indicando el número de experimento.

```
python train_cv_svm_with_patches.py 1
```

Finalmente, para entrenar el modelo utilizando todos los datos es necesario ejecutar el script `/src/fisher/test_svm_with_patches.py` indicando como parámetros el número de PCA, GMM y los parámetros del clasificador SVM (C, kernel y gamma). Este script genera el modelo con todos los datos y prueba el modelo resultante en el conjunto de prueba.

```
python test_svm_with_patches.py 64 16 1 rbf 0.1
```

### Vectores de Fisher a partir de los descriptores (ResNet-50-FV)

El proceso consiste en procesar cada una de las imágenes por la red previamente entrenada, ResNet-50-FT y obtener como salida los descriptores del quinto bloque convolucional. Dado que las imágenes procesadas tienen un tamaño de 224x224, se obtiene como resultado un vector de dimensión (7x7x2048) para cada muestra. Con estos vectores se entrenan un modelo de PCA y de mezclas gaussianas para obtener los FV. Finalmente con los FV se entrena un modelo de SVM. Los parámetros utilizados son los mismos que se usaron en el entrenamiento del modelo ResNet-50-FV-CM.

El entrenamiento con validación cruzada se realiza en el script `/src/fisher/train_cv_svm.py`, el cual requiere el número de experimento a ejecutar.

```
python train_cv_svm.py 1
```

Al igual que el modelo de Vectores de Fisher con muestras, para entrenar el modelo utilizando todos los datos es necesario ejecutar el script `/src/fisher/test_svm.py` indicando como parámetros el número de PCA, GMM y los parámetros del clasificador SVM (C, kernel y gamma). Este script genera el modelo con todos los datos y prueba el modelo resultante en el conjunto de prueba.

```
python test_svm.py 64 32 10 rbf 0.1
```

### Ensamble (FV-Ensamble)

La última prueba realizada consiste en un ensamble de los modelos entrenados de ResNet-50-FV-CM y ResNet-50-FV. El script `/src/fisher/ensamble.py` se encarga de ejecutar este modelo sobre el conjunto de prueba.

```
python ensamble.py
```

## Autores ✒️

* **Cristian Salto** - *Trabajo Inicial* - [clsalto](https://gitlab.com/clsalto)